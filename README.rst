Django PowerDNS
===============

Welcome to the PowerDNS module for Django.

This module allows easy administration of PowerDNS records stored in an SQL database by leveraging
the standard Django Admin module. You may also use the Django PowerDNS module as part of a larger
project to programatically modify your DNS records.
